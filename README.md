# Apartment Listing Scraper 
This script is used for scaping websites for retrieving useful information during an apartment search. 
It will scrape and store the data into a CSV file of your choosing. It will also generate scores based on 
a matrix of criteria that you configure. 

Current Supported Websites: 

* apartments.com (Full support)
* craigslist.com (Partial support)

## Requirements 

You will need:

- Python 3.x 
- Pipenv 2018.11.26 or higher. 

## How to Use

From the project directory install any dependencies

```
pipenv install 
```

Run the shell

```
pipenv shell 
```

With the shell still running and from outside the project directory execute the script. 

```
python apartments-scraper [source] [destination.csv]
```

### Arguments: 
**Source:** The website source. This can be a URL or a text file of multiple URLs separated by a new line. 

**Destination:** 
The destination where you want to save the results. This should be a CSV file. 

## Configuration
### Fields 
You can choose which fields you want to scrape in config.yaml. 

The following fields are supported: 

* Title 
* Rent
* Deposit
* Bedrooms
* Bathrooms
* SqFt
* PetPolicy
* Outdoor
* Parking

### Matrix Criteria
This script provides support for setting criteria that is important to you. It will then
calculate and score each row in the result based on that criteria. 
For example, if price is the most important factor to you then you would assign that 
a high weight and the lowest price would bubble to the top of the list. 

You can combine these criteria to create highly tailored lists of results based on your
preferences. 

**Weight**: 
The importance of this criteria, the higher the value the higher the score. It is 
recommended for all fields weights combined equal 1.0 so that you get a percentage score. 

**Type**: 
The datatype, this determines how to calculate the value. 

Current supported datatypes: 

* Number: Any numerical value, calculates highest or lowest value. 
* Bool: True or False
* Value: Searches for a specific value

**Op**: 
The operation, this is a modifer for the datatype, this changes based on what datatype is chosen. 

Number: asc or desc

* asc: The lowest number gets the highest points. This is useful for finding the lowest price. 
* desc: The highest number gets the highest points. Useful for number of bathrooms or bedrooms. 

Bool: True or False

* True: Set to true when you want a true value to get the highest points. They allow pets for example. 
* False: Set to false when you want a false value to get the highest poits. 

Value: 

* Create a search string that you want to search for. If it matches then it gets the highest points. 
