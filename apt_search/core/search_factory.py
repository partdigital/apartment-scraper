import re
import sys
import os
import requests
import urllib

class SearchFactory:
    @staticmethod
    def load(url):
        # TODO: Make this more dynamic. 
        mappings = [
            {"url": "apartments.com", "source": "apartments"},
            {"url": "craigslist.org", "source": "craigslist"},
        ]
        for mapping in mappings:
            pattern = mapping.get('url')
            if re.search(pattern, url):
                source = mapping.get("source")

        if source == "craigslist":
            from apt_search.sources.craigslist.search import CraigslistSearch
            search = CraigslistSearch(url)
        elif source == "apartments":
            from apt_search.sources.apartments.search import AptSearch 
            search = AptSearch(url)

        return search