#!/usr/bin/env python

import re
import sys
import datetime
import requests
import os

import urllib
import yaml
from definitions import ROOT_DIR

class Config:
    
    def __init__(self):
        
        self.config = {}

    def load(self): 
        config_file = os.path.join(ROOT_DIR, "config.yaml")
        with open(config_file) as f:
            self.config = yaml.safe_load(f)

        return self.config
        
            

        