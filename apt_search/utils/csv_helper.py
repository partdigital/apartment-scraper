#!/usr/bin/env python

import csv
import json
import re
import sys
import datetime
import requests
import os
import urllib

# TODO: Refactor this interface. 
class CsvHelper:
    
    def __init__(self):
        
        # The fields that will be output. 
        # Default are fields that are scraped but can be overwritten.
        self.fields = [] 
        
        self.data = []


    def getFields(self):
        # If fields are not set, then get the first row of the data 
        # and use the dict keys. 
        if len(self.fields) == 0:
            self.fields = list(self.data[0].keys())

        return self.fields
    
    def read(self, filename):
        data = []
        with open(filename, mode='r') as file:
            dict_data = csv.DictReader(file)
            for row in dict_data:
                data.append(row)
        
        return data

    def append(self, filename):
         # open file for writing
        kwargs = {'newline': ''}
        file = open(filename, 'a', **kwargs)

        try:
            writer = csv.writer(file)
            fields = self.getFields()

            # Write data to file. 
            for row in self.data:
                data = []
                for key in fields:
                    if row[key] is not None:
                        data.append(row[key])
                    else:
                        data.append("")
                        
                writer.writerow(data)    

        finally:
            file.close() 

    def write(self, filename):
        # open file for writing
        kwargs = {'newline': ''}
        file = open(filename, 'wt', **kwargs)

        try:
            writer = csv.writer(file)
            fields = self.getFields()

            # Get header from list of fields. 
            writer.writerow(fields)

            # Write data to file. 
            for row in self.data:
                data = []
                for key in fields:
                    if row[key] is not None:
                        data.append(row[key])
                    else:
                        data.append("")
                        
                writer.writerow(data)    

        finally:
            file.close()