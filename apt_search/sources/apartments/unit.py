#!/usr/bin/env python 

from apt_search.core.unit import Unit

class AptUnit(Unit):

    def searchDescription(self, value):
        # TODO: Ths is slow!
        data = []
        s = " "
        obj = self.unit.find('section', class_='descriptionSection')
        return self.matchText(obj, value) 
    
    # Get Url.
    def getUrl(self):
        return self.url
        
     # Get the title of the property. 
    def getTitle(self):
        obj = self.unit.find('h1', class_='propertyName')
        if obj is not None:
            title = obj.getText()
            title = self.sanitize(title)
            return title
        
    # Get the rent of the property.
    def getRent(self):        
        obj = self.unit.find('td', class_='rent')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getDeposit(self):        
        obj = self.unit.find('td', class_='deposit')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data     
        
    def getBedrooms(self):        
        obj = self.unit.find('tr', {'data-beds': True})
        if obj is not None:
            return self.sanitize(obj['data-beds'])
    
    def getBathrooms(self):        
        obj = self.unit.find('tr', {'data-baths': True})
        if obj is not None:
            return self.sanitize(obj['data-baths'])
        
    def getSqFt(self): 
        obj = self.unit.find('td', class_='sqft')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getAvailability(self):        
        obj = self.unit.find('td', class_='available')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getUnitNum(self):        
        obj = self.unit.find('td', class_='unit')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getUnitSqft(self):        
        obj = self.unit.find('td', class_='sqft')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getLease(self):        
        obj = self.unit.find('td', class_='lease')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
    def getPetPolicy(self):   
        obj = self.unit.find('div', class_='petPolicyDetails')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
       
    def getOutdoor(self):
        # TODO: Ths is slow!
        keywords = ["porch", "deck", "balcony", "yard"]
        return self.searchDescription(keywords)
        
    def getParking(self):
        obj = self.unit.find('div', class_='parkingDetails')
        if obj is not None:
            data = obj.getText()
            data = self.sanitize(data)
            return data
        
        obj = self.unit.find('section', class_='descriptionSection')
        keywords = ["parking for rent", "parking for a fee", "parking off-street", "parking space included"]
        return self.matchText(obj, keywords, True)    
   